﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CatsServ
{
    public class Service : ServiceBase
    {
        public Service()
        {
            ServiceName = ServiceName;
        }

        protected override void OnStart(string[] args)
        {
            var t = new Thread(Program.Start);
            t.IsBackground = true;
            t.Start(args);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            if (!Environment.UserInteractive)
            {
                Console.WriteLine("Starting as a service...");
                using (var service = new Service())
                {
                    ServiceBase.Run(service);
                }                   
            }
            else
            {
                Start(args);
            }
        }

        public static void Start(object state)
        {
            var environmentVars = Environment.GetEnvironmentVariables().Keys;

            foreach(string item in environmentVars)
            {
                NLog.LogManager.GetCurrentClassLogger().Info($"item = {item} ({Environment.GetEnvironmentVariable(item)})");
            }

            NLog.LogManager.GetCurrentClassLogger().Info(Path.GetFullPath("%VSAPPIDDIR%"));

            Console.WriteLine("Dod doffкуе Nuland g");
            Console.WriteLine("Donald Tramp rtyu п fsdf");
            Console.WriteLine("RGHTRККК");
            Console.WriteLine("The");
        }
    }

    [RunInstaller(true)]
    public class WindowsServiceInstaller : Installer
    {
        public WindowsServiceInstaller()
        {
            var serviceProcessInstaller = new ServiceProcessInstaller();
            var serviceInstaller = new ServiceInstaller();

            //# Service Account Information
            serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
            serviceProcessInstaller.Username = null;
            serviceProcessInstaller.Password = null;

            //# Service Information
            serviceInstaller.DisplayName = "Z-PRICE CatsServ";
            serviceInstaller.StartType = ServiceStartMode.Automatic;


            //# This must be identical to the WindowsService.ServiceBase name
            //# set in the constructor of WindowsService.cs
            serviceInstaller.ServiceName = "Z-PRICE CatsServ";

            Installers.Add(serviceProcessInstaller);
            Installers.Add(serviceInstaller);
        }

    }
}
